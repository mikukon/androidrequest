/*
 * Request.cpp
 *
 *  Created on: 2012-6-21
 *      Author: amos
 */


#include "Request.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <arpa/inet.h>

Request::Request(){

}

Request::~Request(){

}

void Request::bufferCreated(struct MemBuffer *buf){
	buf->size = MEM_BUFFER_SIZE;
	buf->buffer = (unsigned char*)malloc(buf->size);
	buf->position = buf->buffer;
}


void Request::bufferGlow(struct MemBuffer *buf){
	uint32_t sz;

	sz = buf->position - buf->buffer;

	buf->size += MEM_BUFFER_SIZE;

	buf->buffer = (unsigned char*)realloc(buf->buffer,buf->size);

	buf->position = buf->buffer+sz;
}

void Request::bufferAddByte(struct MemBuffer *buf,uint8_t byt){
	if( (size_t)(buf->position-buf->buffer) >= buf->size){
		bufferGlow(buf);
	}
	*(buf->position++) = byt;
}

void Request::bufferAddBuf(struct MemBuffer *buf,uint8_t *buffer,size_t size){

	while((size_t)(buf->position-buf->buffer)+size>=buf->size){
		bufferGlow(buf);
	}

	memcpy(buf->position,buffer,size);

	buf->position += size;
}


int Request::sendRequest(int isGet,const char* url, char* headerSend,  char* headerRecvice, char* msg){

	struct HttpRequest req;

	memset(&req,0,sizeof(req));

	int i,ret;

	char* buffer;

	if(isGet){
		ret = sendHttp(url,NULL,NULL,0,&req);
	}
	else{
		i = strlen(headerSend);

		buffer = (char*)malloc(1+i);

		strcpy(buffer,headerSend);

		ret = sendHttp( url,
		            "Content-Type: application/x-www-form-urlencoded\r\n",
		            (unsigned char*)buffer,
		            i,
		            &req);

		free(buffer);
	}

	if(!ret){
		return FALSE;
	}
	else{

		headerSend        = req.HttpSend;
		headerRecvice     = req.HttpRecevice;
		msg           = req.message;

		printf("send:\n%s\n\nrecvices:\n%s\n\nmsg:\n%s\n",headerSend,headerRecvice,msg);

		free(req.HttpSend);
		free(req.HttpRecevice);
		free(req.message);

		return TRUE;
	}
}

uint32_t Request::getHostByName(const char* name){

	struct hostent *host;

	host = gethostbyname(name);

	uint32_t addr;

	char str[32];

	const char *tmp;

	tmp = inet_ntop(host->h_addrtype,*host->h_addr_list,str,sizeof(str));

	addr = inet_addr(tmp);

	return addr;
}


void Request::sendString(int sockId,const char* str){
	send(sockId,str,strlen(str),0);
}

int Request::sendHttp(const char* url, char* headerRecevice,unsigned char* post,int length,struct HttpRequest* req){

	struct sockaddr_in sin;

	int socketId;

	char	buffer[512];

	char	protocol[20],host[256],request[1024];

	int l,port,chars,err;

	struct MemBuffer headersBuffer,msgBuffer;

	char headerSend[1024];

	int 	done;

	parseUrl(url,protocol,host,request,&port);

	printf("protocol header:%s, host:%s, request:%s ,port:%d\n",protocol, host, request,port);

	socketId = socket(AF_INET,SOCK_STREAM,NULL);

//	if(socketId == NULL){
//		return -1;
//	}

	memset(&sin,0,sizeof(sin));

	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = getHostByName(host);

	int ret = connect(socketId,(struct sockaddr*)&sin,sizeof(sin));

	if(ret!=0){
		printf("connect failed !\n");
		close(socketId);
		return FALSE;
	}


	//start send int http procotol to server
	// 1.
	if(!*request){
		strcpy(request,"/");
	}

	// 2.
	if(post == NULL){
		sprintf(headerSend,
				"GET %s HTTP/1.1\r\n"
//				"Accept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/vnd.ms-excel, application/msword, application/vnd.ms-powerpoint, */*\r\n"
//				"Accept-Language: en-us\r\n"
//				"Accept-Encoding: gzip, default\r\n"
				"Accept: */*\r\n"
				"User-Agent: miku\r\n"
				"Host: %s\r\n"
				"Connection: Close\r\n\r\n"
				,request,host);
	}
	else{
		sprintf(headerSend,
				"POST %s HTTP/1.1\r\n"
//				"Accept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/vnd.ms-excel, application/msword, application/vnd.ms-powerpoint, */*\r\n"
//				"Accept-Language: en-us\r\n"
//				"Accept-Encoding: gzip, default\r\n"
				"Accept: */*\r\n"
				"User-Agent: miku\r\n"
				"Host: %s\r\n"
				"Connection: Close\r\n\r\n"
				,request,host);
	}

	// 3.
	sendString(socketId,headerSend);

	req->HttpSend = (char*)malloc(sizeof(char*)*strlen(headerSend));
	strcpy(req->HttpSend,headerSend);

	// 4.
	bufferCreated(&headersBuffer);

	chars = 0;
	done = FALSE;

	while(!done)
	{
		l = recv(socketId,buffer,1,0);
		if(l<0)
			done=TRUE;

		switch(*buffer)
		{
		case '\r':
		break;
		case '\n':
		if(chars==0)
			done = TRUE;
		chars=0;
		break;
		default:
			chars++;
			break;
		}

		bufferAddByte(&headersBuffer,*buffer);
	}

	req->HttpRecevice  = (char*) headersBuffer.buffer;

//	*(headersBuffer.position) = '\0';

	bufferCreated(&msgBuffer);                            // Now read the HTTP body

	do
	{
		l = recv(socketId,buffer,sizeof(buffer)-1,0);

		if(l<0)
			break;

//		*(buffer+l)='\0';

		bufferAddBuf(&msgBuffer, (unsigned char*)&buffer, l);

	} while(l>0);

//	*(msgBuffer.position) = '\0';

	req->message = (char*) msgBuffer.buffer;

	req->msgLength = msgBuffer.position - msgBuffer.buffer;

	close(socketId);

	return TRUE;
}

char* Request::upstr(char* str){
	while(*str != '\0'){
		if(*str >= 'a' && *str<='z'){
			*str -= 32;
		}
		++str;
	}
	return str;
}

/**
 * etc.
 * url:		http://www.localhost.com:80/index.html
 * protocol:http
 * host:	www.localhost.com:80
 * request:	/index.html
 * port:	80
 **/
void Request::parseUrl(const char* url, char* protocol, char* host, char* request,int* port){

	char* tmp;

	tmp = strdup(url);

	char* ptr = strstr(tmp,"://");

	if(!ptr){
		printf("Bad url !");
		free(tmp);
		return;
	}

	*(ptr) = '\0';

	strcpy(protocol,tmp);
	upstr(protocol);

	if(*(ptr+1) == '/' && *(ptr+2) == '/'){
		ptr+=3;
	}

	char* ptr2 = strchr(ptr,'/');

	*ptr2 = '\0';

	strcpy(host,ptr);

	char* ptr3 = strchr(host,':');

	if(!ptr3){
		*port = 80;
	}
	else{
		*port = atoi(ptr3+1);
	}

	strcpy(request,url+(ptr2-tmp));

	free(tmp);
}

