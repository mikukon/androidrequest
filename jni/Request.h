/*
 * Request.h
 *
 *  Created on: 2012-6-21
 *      Author: amos
 */

#ifndef REQUEST_H_
#define REQUEST_H_


#include <string.h>

struct HttpRequest{
	char* HttpSend;
	char* HttpRecevice;
	char* message;
	long  msgLength;
};

struct MemBuffer{
	unsigned char* 	buffer;
	unsigned char* 	position;
	unsigned int	size;
};

typedef unsigned int uint32_t;
typedef unsigned char uint8_t;
typedef unsigned int size_t;

#define MEM_BUFFER_SIZE	10
#define TRUE	1
#define FALSE	0

class Request{

public:
	Request();

	virtual ~Request();

private:

	int sendHttp(const char* url,
			char* headerRecevice,
			unsigned char* post,
			int length,
			struct HttpRequest* req);

	void sendString(int sockId,const char* str);

	void bufferCreated(struct MemBuffer *buf);

	void bufferGlow(struct MemBuffer *buf);

	void bufferAddByte(struct MemBuffer *buf,uint8_t byt);

	void bufferAddBuf(struct MemBuffer *buf,uint8_t *buffer,size_t size);

	void parseUrl(const char* url, char* protocol, char* host, char*request,int* port);

	char* upstr(char* str);

	uint32_t getHostByName(const char* name);

public:

	int sendRequest(int isGet,
			const char* url,
			char* headerSend,
			char* headerRecvicer,
			char* msg);
};

#endif /* REQUEST_H_ */
